browser.commands.onCommand.addListener((command) => {
  if (command === "quit_command") {
    browser.windows.getAll().then((wins) => {
      for (let win of wins) {
        browser.windows.remove(win.id);
      }
    });
  }
});