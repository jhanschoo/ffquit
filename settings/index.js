browser.commands.getAll().then(cmds => {
  let cmd = cmds[0];
  document.querySelector("#key-command").value = cmd.shortcut;
});

document.querySelector("#set").onclick = () => {
  browser.commands.update({
    name: "quit_command",
    shortcut: document.querySelector("#key-command").value
  });
};

document.querySelector("#reset").onclick = () => {
  browser.commands.reset("quit_command");
};